# This is provided solely for purposes of `make install'

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
SHAREDIR ?= $(PREFIX)/share
MANDIR ?= $(SHAREDIR)/man

all:

.PHONY: install
install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f llf.lua $(DESTDIR)$(BINDIR)/llf
	mkdir -p $(DESTDIR)$(SHAREDIR)/llf
	cp -f llfrc.lua $(DESTDIR)$(SHAREDIR)/llf/llfrc.lua
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp -f llf.1 $(DESTDIR)$(MANDIR)/man1/llf.1

.PHONY: uninstall
uninstall:
	cd $(DESTDIR)$(BINDIR) && rm -f llf
	cd $(DESTDIR)$(MANDIR)/man1 && rm -f llf.1
	cd $(DESTDIR)$(SHAREDIR) && rm llf/llfrc.lua && rm -r llf
