#!/usr/bin/env lua

local lpeg = require('lpeg')
local wcwidth = require('wcwidth')
local utf8 = require('utf8')

local V, R, P, S, C, Ct, Cg, Cmt, Cb =
        lpeg.V, lpeg.R, lpeg.P, lpeg.S, lpeg.C, lpeg.Ct, lpeg.Cg, lpeg.Cmt,
        lpeg.Cb

-- Configuration

function to_set (l)
        if not l then return {} end
        s = {}
        for _, v in ipairs(l) do s[v] = true end
        return s
end

function load_conf (p) pcall(function () conf = dofile(p) end) end

conf = nil

load_conf('/usr/share/llf/llfrc.lua')

if os.getenv('HOME') then
        load_conf(os.getenv('HOME')..'/.config/llf/llfrc.lua')
end

if os.getenv('XDG_CONFIG_HOME') then
        load_conf(os.getenv('XDG_CONFIG_HOME')..'/llf/llfrc.lua')
end

if #arg == 0 then
elseif #arg == 2 and arg[1] == '-c' then
        if pcall(function () conf = dofile(arg[2]) end) then
        else
                io.stderr:write("couldn't load config file "..arg[2].."\n")
                os.exit(1)
        end
else
        io.write('Usage: '..arg[0]..' [ -c /path/to/config/file ]\n')
        os.exit(0)
end

if not conf then
        io.stderr:write("config file appears malformed\n")
        os.exit(1)
end

conf.inline_environments = to_set(conf.inline_environments)
conf.own_line_controlseqs = to_set(conf.own_line_controlseqs)
conf.start_line_controlseqs = to_set(conf.start_line_controlseqs)
conf.tabular_like_environments = to_set(conf.tabular_like_environments)
conf.own_paragraphs = to_set(conf.own_paragraphs)
conf.indent_incrementers = to_set(conf.indent_incrementers)
conf.indent_decrementers = to_set(conf.indent_decrementers)
conf.max_len = math.max(conf.max_len, 2)

-- build grammar
function f (s, i, delim, c) return delim == c end

constructed_verbatims = (Cg("\\verb", "beginbit") *
                         Cg(V"verbposdelims", "delim") * 
                         Cg((P(1) - Cmt(Cb("delim") * C(1), f))^0, "content") *
                         Cg(Cmt(Cb("delim") * C(1), f), "endbit"))
for _, n in ipairs(conf.verbatim_like_environments) do
        constructed_verbatims = constructed_verbatims + 
                   (Cg("\\begin{"..n.."}", "beginbit") *
                    Cg((P(1) - #P("\\end{"..n.."}"))^0, "content") *
                    Cg("\\end{"..n.."}", "endbit"))
end

texish = P{
        "document";
        ws = S(" \t\v\n"),
        comment = (P"%" * (P(1) - P"\n")^0),
        controlname = C((P"left" + P"right") *
                        ((P(1) - P"\\") + (V"controlseq"))) +
                      (R"az" + R"AZ" + R"09" + "@")^1 * P"*" *
                        (#(1 - (R"az" + R"AZ" + R"09" + S"@*") + -1)) +
                      (R"az" + R"AZ" + R"09" + "@")^1 *
                        (#(1 - (R"az" + R"AZ" + R"09") + -1)),
        controlseq = Ct(P"\\" * Cg(V"controlname", "name") *
                        Cg(Ct((V"subbody")^0), "args")),
        verbposdelims = P(P(1) - P"*"),
        verbdelim = C(V"verbposdelims"),
        verbatimbody = Ct((Cg("%noformat{\n", "beginbit") *
                           Cg((P(1) - #P"%}noformat\n")^0, "content") *
                           Cg("%}noformat", "endbit") * #P"\n") + 
                          constructed_verbatims),
        specialcontrol = Ct(P"\\" * Cg(S"\\ \n%+,/:;=\"\'-{}$^_`~#&[]()!|", "name")),
        word = S"[]" + (1 - S(" \t\n\v\\%{}[]"))^1,
        wordnb = (1 - S(" \t\n\v\\%{}[]"))^1,
        subbody = Ct(Cg(P"{", "l") * Cg(V"body", "body") * Cg(P"}", "r")) +
                  Ct(Cg(P"[", "l") * Cg(V"bodynb", "body") * Cg(P"]", "r")),
        subbodynb = Ct(Cg(P"{", "l") * Cg(V"body", "body") * Cg(P"}", "r")),
        body = Ct(Ct(Cg(V"verbatimbody", "verbatim") +
                        Cg(V"comment", "comment") +
                        Cg(V"ws"^1, "whitespace") +
                        Cg(V"subbody", "subbody") +
                        Cg(V"specialcontrol", "controlseq") +
                        Cg(V"controlseq", "controlseq") +
                        Cg(V"word", "word"))^0),
        bodynb = Ct(Ct(Cg(V"verbatimbody", "verbatim") +
                       Cg(V"comment", "comment") +
                       Cg(V"ws"^1, "whitespace") +
                       Cg(V"subbody", "subbody") +
                       Cg(V"specialcontrol", "controlseq") +
                       Cg(V"controlseq", "controlseq") +
                       Cg(V"wordnb", "word"))^0),
        document = V"body" * (-1)
}

-- state
prog_state = {
        indent_level = 0,
        cell_pos = 0,
        env_depth = 0,
        tabular_depth = 0,
        printed_anything = false,
        sp_before_next = false,
        nl_before_next = 0,
        next_chunk = "",
        next_chunk_indent = 0,
}

-- put enough spaces to start a new line at proper indentation
function indent_to(state)
        i = state.indent_level
        if state.next_chunk ~= "" then
                i = state.next_chunk_indent
        end
        l = (i * 2) % conf.max_len
        io.write(string.rep(" ", l))

        state.cell_pos = l
        state.sp_before_next = false
end

-- calculate (by wcwidth()) cells needed to display a string
function display_width(s)
        local len = 0
        for _, rune in utf8.codes(s) do
                local l = wcwidth(rune)
                if l >= 0 then
                        len = len + l
                end
        end
        return len
end

-- handle seeing \begin{something}
function begin_env(state, e)
        state.env_depth = state.env_depth + 1
        state.indent_level = state.indent_level + 1
        if conf.tabular_like_environments[e] and
           state.tabular_depth == 0 then
                state.tabular_depth = state.env_depth
        end
        if not conf.inline_environments[e] then
                state.nl_before_next = math.max(state.nl_before_next, 1)
        end
end

-- handle seeing \end{something}
function end_env(state, e)
        state.env_depth = state.env_depth - 1
        if state.tabular_depth > state.env_depth then
                state.tabular_depth = 0
        end
        state.indent_level = state.indent_level - 1
        if not conf.inline_environments[e] then
                state.nl_before_next = math.max(state.nl_before_next, 1)
        end
end

function indent_check(state, c)
        if conf.indent_incrementers[c.name] then
                flush_chunk(state)
                state.indent_level = state.indent_level + 1
        end
end

function deindent_check(state, c)
        if conf.indent_decrementers[c.name] then
                flush_chunk(state)
                state.indent_level = state.indent_level - 1
        end
end

-- handle some control sequences starting their own lines
function ensure_start_line(state, c)
        if conf.start_line_controlseqs[c.name] then
                flush_chunk(state)
                state.nl_before_next = math.max(state.nl_before_next, 1)
        end
end

-- handle some control sequences being on their own lines
function ensure_own_line(state, c)
        if c.name == "begin" or c.name == "end" then
                if c.args and c.args[1] and c.args[1].body and
                   c.args[1].body and c.args[1].body[1] then
                        e = c.args[1].body[1].word
                        if type(e) == "string" and
                           conf.inline_environments[e] then
                                return
                        end
                end
                flush_chunk(state)
                state.nl_before_next = math.max(state.nl_before_next, 1)
        end

        if conf.own_line_controlseqs[c.name] then
                flush_chunk(state)
                state.nl_before_next = math.max(state.nl_before_next, 1)
        end

        if conf.own_paragraphs[c.name] then
                flush_chunk(state)
                state.nl_before_next = math.max(state.nl_before_next, 2)
        end
end

-- print something to the screen, properly indented
function write_out_string(state, s)
        if state.nl_before_next > 0 then
                if state.printed_anything then
                        io.write(string.rep("\n", state.nl_before_next))
                        indent_to(state)
                end
                state.sp_before_next = false
        end

        spacelen = (state.sp_before_next and 1) or 0

        slen = display_width(s)

        projected_cell_pos = state.cell_pos + spacelen + slen
        if_newlined = ((state.indent_level * 2) % conf.max_len) + spacelen +
                        slen
        if (projected_cell_pos > conf.max_len) and
           (if_newlined <= conf.max_len) and
           (state.tabular_depth == 0) then
                io.write("\n")
                cell_pos = 0
                spacelen = 0
                indent_to(state)
        end

        if spacelen > 0 then
                io.write(" ")
                state.cell_pos = state.cell_pos + 1
        end

        io.write(s)
        state.printed_anything = true
        state.cell_pos = state.cell_pos + slen
        state.nl_before_next = 0
        state.sp_before_next = false
end

-- commit a chunk of text to be displayed
function commit_string(state, s)
        if state.next_chunk == "" then
                state.next_chunk_indent = state.indent_level
        end
        state.next_chunk = state.next_chunk..s
end

-- flush out the last chunk completely
function flush_chunk(state)
        if state.next_chunk == "" then return end
        write_out_string(state, state.next_chunk)
        state.next_chunk = ""
end

-- handle whitespace
function p_whitespace(state, w)
        flush_chunk(state)
        nls = 0
        for i = 1, string.len(w), 1 do
                if string.sub(w, i, i) == "\n" then
                        nls = nls + 1
                else
                        state.sp_before_next = true
                end
        end

        if nls > 0 and state.tabular_depth > 0 then
                state.nl_before_next = math.max(state.nl_before_next, 1)
        elseif nls == 1 then
                state.sp_before_next = true
        elseif nls >= 2 then
                state.nl_before_next = math.max(state.nl_before_next, 2)
        end
end

-- print out a control sequence, and handle indentation
function p_controlseq(state, c)
        deindent_check(state, c)
        if c.name == "end" and c.args and c.args[1] and
           c.args[1].body and c.args[1].body[1] and
           c.args[1].body[1].word then
                flush_chunk(state)
                end_env(state, c.args[1].body[1].word)
        end

        ensure_start_line(state, c)
        ensure_own_line(state, c)

        if c.name == "\n" then
                commit_string(state, "\\")
                flush_chunk(state)
                state.nl_before_next = math.max(state.nl_before_next, 1)
                return
        end

        commit_string(state, "\\"..c.name)
        if not c.args then c.args = {} end
        for _, a in ipairs(c.args) do
                p_subbody(state, a)
        end

        indent_check(state, c)
        ensure_own_line(state, c)
        if c.name == "begin" and c.args and c.args[1] and
           c.args[1].body and c.args[1].body[1] and
           c.args[1].body[1].word then
                begin_env(state, c.args[1].body[1].word)
        end
end

-- print out a body that was enclosed by { and }
function p_subbody(state, b)
        commit_string(state, b.l)
        state.indent_level = state.indent_level + 1
        p_body(state, b.body)
        state.indent_level = state.indent_level - 1
        commit_string(state, b.r)
end

-- a verbatim environment
function p_verbatim(state, v)
        -- XXX: fix the grammar that caused this mess
        control = nil
        comment = false
        
        if v.beginbit == "\\begin{verbatim}" then
                control = {name="begin", body="verbatim"}
        elseif v.beginbit == "\\begin{Verbatim}" then
                control = {name="begin", body="Verbatim"}
        elseif v.beginbit == "\\begin{alltt}" then
                control = {name="begin", body="alltt"}
        elseif v.beginbit == "\\verb" then
                control = {name="verb"}
        elseif string.sub(v.beginbit, 1, 1) == "%" then
                comment = true
        end

        if control then ensure_own_line(state, control) end

        first_nl_i, first_nl_j = string.find(v.content, "\n")

        s = v.beginbit
        if v.delim then s = s..v.delim end
        if first_nl_i then
                s = s..string.sub(v.content, 1, first_nl_i - 1)
                v.content = string.sub(v.content, first_nl_i, -1)
        else
                s = s..v.content
                v.content = ""
        end
        commit_string(state, s)
        flush_chunk(state)
        io.write(v.content)
        io.write(v.endbit)

        if comment then
                state.nl_before_next = math.max(state.nl_before_next, 1)
        end

        rest = v.content..v.endbit
        last_nl = -1
        while true do
                i = string.find(rest, "\n", last_nl + 1)
                if i == nil then break end
                last_nl = i
        end

        last_line_len = display_width(string.sub(rest, last_nl + 1, -1))
        if last_nl >= 0 then
                state.cell_pos = last_line_len
        else
                state.cell_pos = state.cell_pos + last_line_len
        end

        if control then
                ensure_own_line(state, control)
        end
end

-- print out a comment
function p_comment(state, c)
        flush_chunk(state)
        write_out_string(state, c)
        state.nl_before_next = math.max(state.nl_before_next, 1)
end

-- print out a document contents
function p_body (state, b)
        if b == nil or b == "" then return end
        for _, bb in ipairs(b) do
                if bb.comment then p_comment(state, bb.comment) end
                if bb.whitespace then p_whitespace(state, bb.whitespace) end
                if bb.controlseq then p_controlseq(state, bb.controlseq) end
                if bb.word then commit_string(state, bb.word) end
                if bb.verbatim then p_verbatim(state, bb.verbatim) end
                if bb.subbody then p_subbody(state, bb.subbody) end
        end
end

-- slurp input
corpus = ""

while true do
        local line = io.read()
        if line == nil then break end
        if corpus == "" then
                corpus = line
        else
                corpus = corpus .. "\n" .. line
        end
end


-- parse, perhaps print
local m = texish:match(corpus)
if not m then
        io.write(corpus)
        io.write("\n")
        io.stderr:write("llf: cannot parse input\n")
        os.exit(1)
else
        p_body(prog_state, m)
        flush_chunk(prog_state)
        io.write("\n")
end
