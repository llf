#!/bin/sh

set -eu

# Download a random, recent paper from arXiv and try to run llf on it.

# Note the most common problems are unmatched {, which LLF rejects,
# or an encoding other than UTF-8. These are not considered errors
# in LLF.
tmpdir=$(mktemp -d)
root=$(dirname $(readlink -f "$0") )
cd "${tmpdir}"

discipline=$(dd if=/dev/urandom count=1 bs=1 2>/dev/null | od -A none -d)
discipline=$(( ${discipline} % 20 ))
case ${discipline} in
0) discipline="astro-ph"; ;;
1) discipline="cond-mat"; ;;
2) discipline="cs"; ;;
3) discipline="econ"; ;;
4) discipline="eess"; ;;
5) discipline="gr-qc"; ;;
6) discipline="hep-ex"; ;;
7) discipline="hep-lat"; ;;
8) discipline="hep-ph"; ;;
9) discipline="hep-th"; ;;
10) discipline="math"; ;;
11) discipline="math-ph"; ;;
12) discipline="nlin"; ;;
13) discipline="nucl-ex"; ;;
14) discipline="nucl-th"; ;;
15) discipline="physics"; ;;
16) discipline="q-bio"; ;;
17) discipline="q-fin"; ;;
18) discipline="quant-ph"; ;;
19) discipline="stat"; ;;
esac

absurl=$(curl -s "http://export.arxiv.org/rss/${discipline}" | grep -Eo '[^"<>]+arxiv.org/abs/[^"<>]+' | sort -R | head -n 1)
srcurl=$(printf %s\\n "${absurl}" | sed -re 's/\/abs\//\/e-print\//')
srcname=$(printf %s\\n "${srcurl}" | sed -re 's/.*\///')
curl -Ls "${srcurl}" -o "${srcname}"
mime=$(file -bi "${srcname}"  | sed -re 's/;.*//')

case ${mime} in
application/x-gzip)
        tar -xf "${srcname}" 2>/dev/null >/dev/null || gunzip -c "${srcname}" > "${srcname}.raw"
        ;;
application/gzip)
        tar -xf "${srcname}" 2>/dev/null >/dev/null || gunzip -c "${srcname}" > "${srcname}.raw"
        ;;
text/x-tex) ;;
application/pdf)
        printf 'No TeX source for %s\n' "${absurl}"
        printf 'tmpdir = %s\n' "${tmpdir}"
        exit 1
        ;;
*)
        printf 'Unknown mimetype %s at %s\n' "${mime}" ${tmpdir}
        printf 'tmpdir = %s\n' "${tmpdir}"
        exit 1
        ;;
esac

oldifs="${IFS}"
IFS="
"
err=0
for f in $(ls); do
        mime=$(file -bi "${f}"  | sed -re 's/;.*//')
        [ "${mime}" = "text/x-tex" ] || continue
        case "${f}" in
        *sty) continue ;;
        *bst) continue ;;
        esac

        "${root}/llf.lua" -c "${root}/llfrc.lua" < "${f}" 2>/dev/null > /dev/null && continue
        err=1
        printf 'LLF could not handle %s\n' "${f}"
done 

if [ "${err}" -eq 0 ]; then
        printf 'All okay for %s\n' "${absurl}"
        rm -rf "${tmpdir}"

        exit 0
else
        printf 'There were some errors for %s\n' "${absurl}"
        printf 'tmpdir = %s\n' "${tmpdir}"
fi
