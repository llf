#!/bin/sh

set -eu

root=$(dirname $(readlink -f "$0") )
tmpdir=$(mktemp -d)
ret=0

cd "${root}"
pat=""
[ "$#" -ge 1 ] && pat="$1"

for inputf in $(ls tests/*${pat}*.input.tex)
do
        nicename="${inputf%.input.tex}"
        outputf="${nicename}.output.tex"
        resultf="${tmpdir}/"$(basename "${outputf}")

        if [ -f "${outputf}" ]
        then
                printf '%s... ' "${nicename}"
                "${root}/llf.lua" -c "${root}/llfrc.lua" < "${inputf}" \
                        2>&1 > "${resultf}"
                if diff -q "${outputf}" "${resultf}" >/dev/null 2>&1
                then
                        printf '\033[1;34mPASS\033[0m\n'
                else
                        ret=1
                        printf '\033[1;31mFAIL\033[0m\n'
                        diff -u "${resultf}" "${outputf}"
                fi
        else
                printf '%s... ' "${nicename}"
                printf '\033[1;33mNO STORED OUTPUT\033[0m\n'
        fi
done

rm -rf "${tmpdir}"

exit "${ret}"
